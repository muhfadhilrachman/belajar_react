import React from "react";
import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./sidebar.css";
const Sidebar = () => {
  const listSidebar = [
    {
      name: "Pengeluaran",
      path: "/pengeluaran",
    },

    {
      name: "pemasukan",
      path: "/pemasukan",
    },
  ];
  return (
    <div xs="2" className="col-2 border-end cuy">
      <ul className="py-5">
        {listSidebar.map((resp, index) => (
          <Link key={index} to={resp.path}>
            <li className="fs-6 mb-4">{resp.name}</li>
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default Sidebar;
