import React from "react";
import { Button } from "react-bootstrap";

const Navbar = () => {
  return (
    <div className="border-bottom p-3 d-flex justify-content-between align-items-center">
      <div>Logo</div>
      <div>
        <Button variant="success">about</Button>
      </div>
    </div>
  );
};

export default Navbar;
