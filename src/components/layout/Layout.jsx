import React from "react";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";

const Layout = ({ children }) => {
  return (
    <div>
      <Navbar />
      <div className="d-flex">
        <Sidebar />
        <div className="p-3">{children}</div>
      </div>
    </div>
  );
};

export default Layout;
