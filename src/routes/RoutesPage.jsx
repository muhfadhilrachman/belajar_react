import React from "react";
import { Route, Router, Routes } from "react-router-dom";
import Layout from "../components/layout/Layout";
import routes from "./routes";

const RoutesPage = () => {
  return (
    <Layout>
      {routes.map((item) => (
        <Routes>
          <Route path={item.path} element={item.element}></Route>;
        </Routes>
      ))}
    </Layout>
  );
};

export default RoutesPage;
