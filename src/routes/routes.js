import ListPengeluaran from "../pages/pengeluaran/ListPengeluaran";
import ListPemasukan from "../pages/pemasukan/ListPemasukan";

const routes = [
  {
    path: "/pemasukan",
    element: <ListPemasukan />,
  },
  {
    path: "/pengeluaran",
    element: <ListPengeluaran />,
  },
];

export default routes;
